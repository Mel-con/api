<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Site
Route::get('/', 'Site\HomeController@index');
Route::post('/customer/index', 'Api\CustomerController@index');
Route::post('/customer/show', 'Api\CustomerController@show');

//API Customer part
Route::post('/customer/create', 'Api\CustomerController@create')->name('customer_create');
Route::post('/customer/edit', 'Api\CustomerController@edit')->name('customer_edit');

//API Payment part
Route::post('/payment/deposit', 'Api\PaymentController@deposit')->name('payment_deposit');
Route::post('/payment/withdraw', 'Api\PaymentController@withdraw')->name('payment_withdraw');
Route::post('/payment/report', 'Api\PaymentController@report');
