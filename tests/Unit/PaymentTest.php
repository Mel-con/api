<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentTest extends TestCase
{
    /**
     * Test payment deposit
     */
    public function testPaymentDeposit()
    {
        $response = $this->call('POST', route('payment_deposit'), [
            'customer_id' => '1',
            'amount' => '100',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Test payment deposit failure
     */
    public function testPaymentDepositFailure()
    {
        $response = $this->call('POST', route('payment_deposit'), [
            'customer_id' => '100',
            'amount' => '100',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * Test payment deposit validation
     */
    public function testPaymentDepositValidation()
    {
        $response = $this->call('POST', route('payment_deposit'), [
            'customer_id' => '1',
            'amount' => '-90',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(422, $response->getStatusCode());
    }

    /**
     * Test payment deposit
     */
    public function testPaymentWithdraw()
    {
        $response = $this->call('POST', route('payment_withdraw'), [
            'customer_id' => '1',
            'amount' => '100',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Test payment deposit failure
     */
    public function testPaymentWithdrawFailure()
    {
        $response = $this->call('POST', route('payment_withdraw'), [
            'customer_id' => '100',
            'amount' => '100',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * Test payment withdraw validation
     */
    public function testPaymentWithdrawValidation()
    {
        $response = $this->call('POST', route('payment_withdraw'), [
            'customer_id' => '1',
            'amount' => '-90',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(422, $response->getStatusCode());
    }
}
