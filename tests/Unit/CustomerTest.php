<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomerTest extends TestCase
{
    /**
     * Customer create test
     */
    public function testCustomerCreate()
    {
        $response = $this->call('POST', route('customer_create'), [
            'gender'=>'Male',
            'first_name'=>'Melkon',
            'last_name'=>'Chilingaryan',
            'email' => 'melkon.chilingaryan99@gmail.com',
            'country' => 'Armenia',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Test customer create validation
     */
    public function testCustomerCreateValidation()
    {
        $response = $this->call('POST', route('customer_create'), [
            'gender'=>'Male',
            'first_name'=>'Melkon',
            'last_name'=>'',
            'email' => 'melkon.chilingaryan@gmail.com',
            'country' => 'Armenia',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(422, $response->getStatusCode());
    }

    /**
     * Test customer edit
     */
    public function testCustomerEdit()
    {
        $response = $this->call('POST', route('customer_edit'), [
            'customer_id'=>'1',
            'gender'=>'Male',
            'first_name'=>'Melkon',
            'last_name'=>'Chilingaryan',
            'email' => 'melkon.chilingaryan@gmail.com',
            'country' => 'Armenia',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Test customer edit failure case
     */
    public function testCustomerEditFailure()
    {
        $response = $this->call('POST', route('customer_edit'), [
            'customer_id'=>'100',
            'gender'=>'Male',
            'first_name'=>'Melkon',
            'last_name'=>'Chilingaryan',
            'email' => 'melkon.chilingaryan99@gmail.com',
            'country' => 'Armenia',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * Test edit customer Validation
     */
    public function testCustomerEditValidation()
    {
        $response = $this->call('POST', route('customer_edit'), [
            'customer_id'=>'1',
            'gender'=>'Male',
            'first_name'=>'',
            'last_name'=>'Chilingaryan',
            'email' => '',
            'country' => 'Armenia',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(422, $response->getStatusCode());
    }
}
