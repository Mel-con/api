<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string country
 * @property string bonus_percent
 */
class Customer extends Model
{
    protected $table = 'customer';

    /**
     * Get customer by id
     *
     * @param $id
     * @return ModelError
     */
    public static function getCustomerById($id)
    {
        $customer = Customer::whereId($id)->first();
        if ($customer) {
            return $customer;
        } else {
            return new ModelError(ModelError::CUSTOMER_NOT_FOUND);
        }
    }

    /**
     * Create customer
     *
     * @param bool $gender
     * @param bool $first_name
     * @param bool $last_name
     * @param bool $email
     * @param bool $country
     * @return Customer
     */
    public static function createCustomer($gender = false, $first_name = false, $last_name = false, $email = false, $country = false)
    {
        $percent = rand(5, 15);

        $customer = new self();
        if (!empty($gender)) {
            $customer->gender = $gender;
        }
        if ($first_name) {
            $customer->first_name = $first_name;
        }
        if ($last_name) {
            $customer->last_name = $last_name;
        }
        if ($email) {
            $customer->email = $email;
        }
        if ($country) {
            $customer->country = $country;
        }

        $customer->bonus_percent = $percent;

        if ($customer->save()) {
            return $customer;
        }
    }


    /**
     * Edit customer
     *
     * @param bool $customer_id
     * @param bool $gender
     * @param bool $first_name
     * @param bool $last_name
     * @param bool $email
     * @param bool $country
     * @return ModelError
     */
    public static function editCustomer($customer_id = false, $gender = false, $first_name = false, $last_name = false, $email = false, $country = false)
    {
        $customer = self::getCustomerById($customer_id);

        if (!ModelError::isError($customer)) {
            if (!empty($gender)) {
                $customer->gender = $gender;
            }
            if ($first_name) {
                $customer->first_name = $first_name;
            }
            if ($last_name) {
                $customer->last_name = $last_name;
            }
            if ($email) {
                $customer->email = $email;
            }
            if ($country) {
                $customer->country = $country;
            }

            if ($customer->save()) {
                return $customer;
            }
        } else {
            return $customer;
        }
    }

}
