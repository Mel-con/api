<?php

namespace App\Http\Models;



class ModelError
{
    const CUSTOMER_NOT_FOUND = 1000; // Customer not found
    const INEFFICIENT_FOUNDS = 2000; // Inefficient founds

    /**
     * ModelError constructor.
     *
     * @param array $code
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Check if object instanceof model error
     *
     * @param $object
     * @return bool
     */
    public static function isError($object)
    {
        return $object instanceof ModelError;
    }

    /**
     * Get error code
     *
     * @return array
     */
    public function getErrorCode()
    {
        return $this->code;
    }
}