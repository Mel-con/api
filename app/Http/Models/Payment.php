<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property integer customer_id
 * @property integer type
 * @property double amount
 */
class Payment extends Model
{
    protected $table = 'payment';

    /**
     * Get customer payments by customer id
     *
     * @param $id
     * @return mixed
     */
    public static function getCustomerPaymentsByCustomerId($id)
    {
        $payment = self::whereCustomerId($id)->whereType(1)->count();
        if ($payment) {
            return $payment;
        }
    }

    /**
     * Create deposit payment by customer id and amount
     *
     * @param bool $customer_id
     * @param bool $amount
     * @return ModelError
     */
    public static function deposit($customer_id = false, $amount = false)
    {
        $customer = Customer::getCustomerById($customer_id);

        if (!ModelError::isError($customer)) {
            $payment = new self();
            if (!empty($customer_id)) {
                $payment->customer_id = $customer_id;
            }
            if ($amount) {
                $payment->amount = $amount;
            }
            $payment->type = 1;
            $payment->save();

            $new_balance = $customer->balance + $amount;
            $customer->balance = $new_balance;
            $customer->save();

            $count = self::getCustomerPaymentsByCustomerId($customer_id);
            if ($count % 3 == 0) {
                $new_bonus_balance = $customer->bonus_balance + $customer->bonus_percent / 100 * $amount;
                $customer->bonus_balance = $new_bonus_balance;
                $customer->save();

                $payment = new self();
                if (!empty($customer_id)) {
                    $payment->customer_id = $customer_id;
                }
                if ($amount) {
                    $payment->amount = $customer->bonus_percent / 100 * $amount;
                }
                $payment->type = 2;
                $payment->save();
            }
            return $payment;
        } else {
            return $customer;
        }
    }

    /**
     * Create withdraw by customer id and amount
     *
     * @param bool $customer_id
     * @param bool $amount
     * @return ModelError
     */
    public static function withdraw($customer_id = false, $amount = false)
    {
        $customer = Customer::getCustomerById($customer_id);

        if (!ModelError::isError($customer)) {
            if ($customer->balance >= $amount) {
                $payment = new self();

                if (!empty($customer_id)) {
                    $payment->customer_id = $customer_id;
                }
                if ($amount) {

                    $payment->amount = -$amount;

                }
                $payment->type = 0;
                $payment->save();

                $new_balance = $customer->balance - $amount;
                $customer->balance = $new_balance;
                $customer->save();
                return $payment;
            } else {
                return new ModelError(ModelError::INEFFICIENT_FOUNDS);
            }
        } else {
            return $customer;
        }
    }

    /**
     * Genereate Report by period
     *
     * @param bool $period
     * @return ModelError
     */
    public static function report($period = false)
    {
        $report = self::leftjoin('customer', 'customer.id', '=', 'payment.customer_id')
            ->where('payment.created_at', '>=', date("Y-m-d", strtotime('-' . $period . 'days')))
            ->select('customer.country as country1', 'payment.type', 'payment.id', 'payment.amount',
                DB::raw('DATE(payment.created_at) as time'),
                DB::raw("(select COUNT(customer.id) from `customer` where country1=`customer`.`country`) as unique_customer"),
                DB::raw("(select COUNT(payment.id) from `payment` left join `customer` on `customer`.`id` = `payment`.`customer_id` where payment.type=1 AND country1=`customer`.`country`) as deposit_count"),
                DB::raw("(select IF( SUM(payment.amount) IS NULL, 0, SUM(payment.amount)) from `payment` left join `customer` on `customer`.`id` = `payment`.`customer_id` where payment.type=1 AND country1=`customer`.`country`) as deposit_sum"),
                DB::raw("(select COUNT(payment.id) from `payment` left join `customer` on `customer`.`id` = `payment`.`customer_id` where payment.type=0 AND country1=`customer`.`country`) as withdraw_count"),
                DB::raw("(select IF( SUM(payment.amount) IS NULL, 0, SUM(payment.amount))from `payment` left join `customer` on `customer`.`id` = `payment`.`customer_id` where payment.type=0 AND country1=`customer`.`country`) as withdraw_sum"))
            ->groupBy('time', 'country')->get();
        return $report;
    }
}
