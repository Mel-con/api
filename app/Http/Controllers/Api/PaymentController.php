<?php

namespace App\Http\Controllers\Api;

use App\Http\Models\Customer;
use App\Http\Models\Payment;
use App\Http\Models\ModelError;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{

    /**
     * Make deposit
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deposit(Request $request)
    {
        if ($request->isMethod('post')) {
            $errors = '';
            $status_code = 500;
            $success = false;
            $success_message = '';

            $customer = Customer::getCustomerById($request->input('customer_id'));

            // Customer Not Found Case
            if (ModelError::isError($customer)) {
                $error_code = $customer->getErrorCode();
                if ($error_code == ModelError::CUSTOMER_NOT_FOUND) {
                    $errors = 'Customer Not Found';
                }
            } else {
                $rules = [
                    'amount' => 'required|integer|min:0'
                ];

                // Validation Failure Case
                $validation = Validator::make($request->all(), $rules);
                if ($validation->fails()) {
                    $errors = json_decode($validation->messages());
                    $status_code = 422;
                    $success = false;
                } // Validation Success Case
                else {
                    $customer_id = $request->input('customer_id');
                    $amount = $request->input('amount');

                    // Make deposit
                    $payment = Payment::deposit($customer_id, $amount);

                    if (!empty($payment)) {
                        $success_message = 'Deposit money successfully done';
                        $status_code = 200;
                        $success = true;
                    }
                }
            }

            return response()->json([
                'success' => $success,
                'errors' => $errors,
                'success_message' => $success_message
            ], $status_code);
        }
    }

    /**
     * Make withdraw
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function withdraw(Request $request)
    {
        if ($request->isMethod('post')) {
            $errors = '';
            $status_code = 500;
            $success = false;
            $success_message = '';

            $customer = Customer::getCustomerById($request->input('customer_id'));

            // Customer Not Found Case
            if (ModelError::isError($customer))
            {
                $error_code = $customer->getErrorCode();
                if ($error_code == ModelError::INEFFICIENT_FOUNDS) {
                    $errors = trans('messages.money_is_no_efficient');
                }
            }
            else {
                $rules = [
                    'amount' => 'required|integer|min:0'
                ];

                // Validation Failure Case
                $validation = Validator::make($request->all(), $rules);
                if ($validation->fails()) {
                    $errors = json_decode($validation->messages());
                    $status_code = 422;
                    $success = false;
                } // Validation Success Case
                else {
                    $customer_id = $request->input('customer_id');
                    $amount = $request->input('amount');

                    // Make withdraw
                    $payment = Payment::withdraw($customer_id, $amount);

                    if (ModelError::isError($payment)) {
                        $error_code = $payment->getErrorCode();
                        if ($error_code == ModelError::INEFFICIENT_FOUNDS) {
                            $errors = trans('messages.money_is_no_efficient');
                            $status_code = 500;
                            $success = false;
                            $success_message = '';
                        }
                    } else {
                        if (!empty($payment)) {
                            $success_message = trans('messages.withdraw_successfully_done');
                            $status_code = 200;
                            $success = true;
                        }
                    }
                }

            }
            return response()->json([
                'success' => $success,
                'errors' => $errors,
                'success_message' => $success_message
            ], $status_code);
        }
    }

    /**
     * Get Report
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function report(Request $request)
    {
        if ($request->isMethod('post')) {
            $errors = '';
            $status_code = 500;
            $success = false;
            $success_message = '';
            $report='';

            $rules = [
                'period' => 'required|integer|min:1'
            ];

            // Validation Failure Case
            $validation = Validator::make($request->all(), $rules);
            if ($validation->fails()) {
                $errors = json_decode($validation->messages());
                $status_code = 422;
                $success = false;
            } // Validation Success Case

                else {
                    $period = $request->input('period');

                    // Generate Report
                    $report = Payment::report($period);

                    if (ModelError::isError($report)) {
                        $error_code = $report->getErrorCode();
                        if ($error_code == ModelError::INEFFICIENT_FOUNDS) {
                            $errors = trans('messages.money_is_no_efficient');
                            $status_code = 500;
                            $success = false;
                            $success_message = '';
                        }
                    } else {
                        if (!empty($report)) {
                            $success_message = trans('messages.withdraw_successfully_done');
                            $status_code = 200;
                            $success = true;
                        }
                    }
                }
                return response()->json([
                    'success' => $success,
                    'errors' => $errors,
                    'success_message' => $success_message,
                    'report' => $report
                ], $status_code);
            }
        }

}

