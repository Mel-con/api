<?php

namespace App\Http\Controllers\Api;

use App\Http\Models\Customer;
use App\Http\Models\ModelError;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Get customer list
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $customers = Customer::get();
        return response()->json([
            'cusromers' => $customers
        ]);
    }

    /**
     * Show customer data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        if ($request->isMethod('post')) {
            $customer = Customer::where('id', $request->input('id'))->first();
            return response()->json([
                'customer' => $customer
            ]);
        }
    }

    /**
     * Create customer
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $errors = '';
            $status_code = 500;
            $success = false;
            $success_message = '';

            // Validation Rules
            $rules = [
                'gender' => 'required|max:10',
                'first_name' => 'required|max:30',
                'last_name' => 'required|max:30',
                'email' => 'required|unique:customer,email|email',
                'country' => 'required|max:30'
            ];

            // Validation Failed Case
            $validation = Validator::make($request->all(), $rules);
            if ($validation->fails()) {
                $errors = json_decode($validation->messages());
                $status_code = 422;
                $success = false;
            } // Validation Success Case
            else {
                $gender = $request->input('gender');
                $first_name = $request->input('first_name');
                $last_name = $request->input('last_name');
                $email = $request->input('email');
                $country = $request->input('country');

                // Create customer
                $customer = Customer::createCustomer($gender, $first_name, $last_name, $email, $country);

                if (!empty($customer)) {
                    $success_message = trans('messages.customer_successfully_created');
                    $status_code = 200;
                    $success = true;
                }
            }

            return response()->json([
                'success' => $success,
                'errors' => $errors,
                'success_message' => $success_message
            ], $status_code);
        }
    }

    /**
     * Update customer
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {

        if ($request->isMethod('post')) {
            $errors = '';
            $status_code = 500;
            $success = false;
            $success_message = '';

            $customer = Customer::getCustomerById($request->input('customer_id'));

            // Customer Not Found Case
            if (ModelError::isError($customer)) {
                $error_code = $customer->getErrorCode();
                if ($error_code == ModelError::CUSTOMER_NOT_FOUND) {
                    $errors = 'Customer Not Found';
                }
            } else {
                $rules = [
                    'gender' => 'required|max:10',
                    'first_name' => 'required|max:30',
                    'last_name' => 'required|max:30',
                    'email' => 'required|email|unique:customer,email,' . $request->input('customer_id'),
                    'country' => 'required|max:30'
                ];

                // Validation Failure Case
                $validation = Validator::make($request->all(), $rules);
                if ($validation->fails()) {
                    $errors = json_decode($validation->messages());
                    $status_code = 422;
                    $success = false;
                } // Validation Success Case
                else {
                    $customer_id = $request->input('customer_id');
                    $gender = $request->input('gender');
                    $first_name = $request->input('first_name');
                    $last_name = $request->input('last_name');
                    $email = $request->input('email');
                    $country = $request->input('country');

                    // Edit customer
                    $customer = Customer::editCustomer($customer_id, $gender, $first_name, $last_name, $email, $country);

                    if (!empty($customer)) {
                        $success_message = trans('messages.customer_successfully_edited');
                        $status_code = 200;
                        $success = true;
                    }
                }
            }

            return response()->json([
                'success' => $success,
                'errors' => $errors,
                'success_message' => $success_message
            ], $status_code);
        }
    }
}
