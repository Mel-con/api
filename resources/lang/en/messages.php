<?php

return [
    'customer_successfully_created' => 'Customer successfully created',
    'customer_successfully_edited' => 'Customer successfully edited',
    'customer_not_found' => 'Customer Not Found',
    'money_is_no_efficient' => 'Money on Balance is not efficient',
    'withdraw_successfully_done' => 'Withdraw money successfully done',
];