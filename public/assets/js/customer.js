$(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
    });

    // Create customer
    function createCustomer(gender, first_name, last_name, email, country) {
        $.ajax({
            url: '/customer/create',
            type: "post",
            dataType: "json",
            data: {
                'gender': gender,
                'first_name': first_name,
                'last_name': last_name,
                'email': email,
                'country': country,
                '_token': $('input[name=_token]').val()
            },
            success: function (data) {
                console.log(data)
            },
            error: function (error) {
                console.log(error.responseText)
            }
        });
    }

    // Edit customer
    function editCustomer(customer_id, gender, first_name, last_name, email, country) {
        $.ajax({
            url: '/customer/edit',
            type: "post",
            dataType: "json",
            data: {
                'customer_id': customer_id,
                'gender': gender,
                'first_name': first_name,
                'last_name': last_name,
                'email': email,
                'country': country,
                '_token': $('input[name=_token]').val()
            },
            success: function (data) {
                console.log(data)
            },
            error: function (error) {
                console.log(error.responseText)
            }
        });
    }

    
    //Function Calls

    //editCustomer(5, 'Male', 'Melkon1', 'Chilingaryan1', 'melkon.chilingaryan@gmail.com','Armenia');
    //createCustomer('Male', 'Melkon', 'Chilingaryan', 'melkon.chilingar3yan1@gmail.com', 'Armenia');

});
