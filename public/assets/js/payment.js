$(function () {
    // Make withdraw
    function withdraw(customer_id,amount) {
        $.ajax({
            url: '/payment/withdraw',
            type: "post",
            dataType: "json",
            data: {'customer_id': customer_id, 'amount': amount, '_token': $('input[name=_token]').val()},
            success: function (data) {
                console.log(data)
            },
            error: function (error) {
                console.log(error.responseText)
            }
        });
    }

    // Make deposit
    function deposit(customer_id,amount) {
        $.ajax({
            url: '/payment/deposit',
            type: "post",
            dataType: "json",
            data: {'customer_id':customer_id,'amount':amount, '_token': $('input[name=_token]').val()},
            success: function(data){
                console.log(data)
            },
            error: function (error) {
                console.log(error.responseText)
            }
        });
    }

    // Make report
    function report(period) {
        $.ajax({
            url: '/payment/report',
            type: "post",
            dataType: "json",
            data: {'period':period, '_token': $('input[name=_token]').val()},
            success: function(data){
                console.log(data)
            },
            error: function (error) {
                console.log(error.responseText)
            }
        });
    }

    //Function Calls

    //deposit(1,100);
    //withdraw(1,50);
    //report(10);
});
