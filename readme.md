**Deployment Steps**

1.	You need XAMPP(LAMP,WAMP) installed on your local Machine/
2.	Clone or download project in \htdocs  directory with name api
3.	Configure hosts and vhosts of your server
4.	In MySQL create database “testapi”
5.	Run composer update (for this go to the project folder) 
In this folder open console (for example git bash) and run
“Composer update” this will create vendor and dependencies
6.	In .env file the database username is ‘root’ and there is no password, if you want to change it you need to change in .env file
7.	Run migrations (for this go to the project folder) 
In this folder open console (for example git bash) and run
“Php artisan migrate” this will create tables in the database
8.	Now you will have system ready for user
9.	Open the URL that you specified in your hosts and vhosts.
10.	I created User interface that You could easily navigate the functionality
11.	You can go to /assets and open customer.js or payment.js and see there test function calls, with uncommenting them you will manually test with JavaScript.
12.	You can open the project containing folder open the console and run “vendor\bin\phpunit” this will run Unit tests.